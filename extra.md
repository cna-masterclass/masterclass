# Extra Exercises #

1. Build your own API container used in [Exercise 4](exercise-04.md)
2. Display images from a CDN service

## Build your API ##

The exercise 4 includes a [`Dockerfile`](exercise-04/api/Dockerfile) to build the the same API container image without using a volume instead embedding the json file within the container. Use this image in the API deployment and get the application working.

Here are the high-level steps that would be involved

1. Build the docker image using the provided [`Dockerfile`](exercise-04/api/Dockerfile)
2. Push to Docker Hub
3. Update the deployment to use this new image
4. Verify everything is working as before

## Host images ##

The Exercise 4 has a `cdn` folder with the images that are referred in the API. You will notice that the UI is already making requests to `/img/<filename.jpg>`. So, make sure that these images are getting served from there.

High-level steps:

1. Build the docker image using the provided [`Dockerfile`](exercise-04/cdn/Dockerfile)
2. Push to Docker Hub
3. Create a new CDN deployment
4. Expose the CDN deployment using a service
5. Update the ingress to add an additional route to the CDN service

If all goes well you should be able to see the following

![Preview Extra](img/preview-extra.png)

## Kubernetes Resources ##

![Extra](img/extra.png)

