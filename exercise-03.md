# Exercise 3 #

## Create a service ##

In this exercise we will create a service and expose it publicly using an ingress so that we can access the webserver that we deployed in the earlier exercise

1. Expose the webserver deployment as a service

    ```sh
    kubectl expose deploy/webserver \
        --port=8080 --target-port=80 \
        --name webserver
    ```

   Alternatively use the yaml service definition [`service.yml`](exercise-03/service.yml) to create the service

    ```sh
    kubectl apply -f exercise-03/service.yml
    ```

   Notice that, although the nginx webserver is started in port `80` we are exposing it on port `8080` on the service

2. Verify the service & edpoint is created

    ``` sh
    kubectl get svc,ep
    ```

## Create an ingress ##

1. Create an `ingress.yml` with the following contents. Make sure to replace the `<namespace>` with your namespace so, that it does not clash with other participants

   ``` yaml
   apiVersion: extensions/v1beta1
   kind: Ingress
   metadata:
     annotations:
       kubernetes.io/ingress.class: nginx
     name: webserver
   spec:
     rules:
     - host: <namespace>.cna.wcloudacademy.com
       http:
         paths:
         - backend:
             serviceName: webserver
             servicePort: 8080
   ```

   Alternatively use the yaml definition [`ingress.yml`](exercise-03/ingress.yml) to create the service

    ```sh
    kubectl apply -f exercise-03/ingress.yml
    ```

2. Access the webserver from [&lt;namespace&gt;.cna.wcloudacademy.com](http://cna.wcloudacademy.com)

## Kubernetes Resources ##

![Exercise 03](img/ex-03.png)
