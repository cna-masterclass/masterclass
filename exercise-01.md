# Exercise 1 #

In this exercise we will write a simple web server in [Node.js][nodejs] and

- run it directly
- build and run in a container
- push to docker registry & run in kubernetes

## Docker Introduction ##

Docker is already available in the Google Cloud Shell VM so, we can use the same docker installation. We will use the docker command line for

1. Type command at the shell prompt

    ``` sh
    docker --version
    ```

   You will see output as something like below. The actual version and build will vary

    ``` text
    Docker version 19.03.1, build 74b1e89
    ```

2. Check if there is any docker image already existing locally

    ``` sh
    docker  images
    ```

   Initially you will see empty listing as we have not built any docker image yet.

**NOTE**: Learn more about the docker command line [here][docker-cli]

## Create Hello world Node.js program ##

In order to containerize a simple program let us first create a simple Hello World program in Node.js. Google Cloud Shell already has Node.js preinstalled and it is a simple light weight interpreted language. You can create a program in any other language that you are comfortable after installing the compiler and runtime libraries in Cloud Shell.

1. We need to create a `hello.js` file in the Cloud Shell with the below content. The program starts HTTP server and listens in port `8080`. When it is invoked it returns the string `'Hello World!'`. You can skip the comment if you are manually typing.

    ```js
    var http = require('http'); //create a http server object:

    var port = process.env.PORT || 8080; // default port 8080

    http.createServer(function (req, res) {
      res.write('Hello World!'); //write a response to the client
      res.end(); //end the response
    }).listen(port, (err) => {
      if ( err ) throw err;
      console.log('Server running at ' + port); // Print in console when server starts
    }); //the server object listens on port 8080
    ```

2. You may use the built-in user friendly Cloud Shell code editor to type the program by selecting the icon in the top right corner

    ![Code Editor](img/code-editor.png)

3. Or alternatively upload the [`hello.js`](exercise-01/hello.js) file through the option by selecting the three dots in the top right corner.

    ![File Upload](img/upload-file.png)

4. Check if the file is created properly by printing the contents using the below command in the shell prompt

    ``` sh
    cat hello.js
    ```

5. Test the program by issuing the following command in the shell prompt

    ``` sh
    node hello.js
    ```

    You should see content below and the prompt will no longer available as the program is running in the foreground.

    ``` text
    Server running at 8080
    ```

6. The hello world program is currently running in the VM that is in Google Cloud and listening to port 8080. In order to test we need to know the server name or the IP address. Cloud shell provides **Web preview** option to access the server from outside the Cloud. Select **Web preview** option

7. Select **Preview on port 8080** in the option shown in **Web preview**.

    ![Web Preview](img/web-preview.png)

8. You should be seeing Hello World in the new browser window.

    ![Preview Hello](img/preview-hello.png)

9. Stop the hello World program by pressing `Ctrl+C` in the shell where the server is running

## Containerizing the Hello World Program ##

In the above step we have created and tested a simple Hello World program. Now let us containerize the same application with the below steps

1. In order to containerize the application, a `Dockerfile` has to be created that should contain instructions for creating container image.

    ``` dockerfile
    FROM node:alpine

    WORKDIR /app

    ADD . /app

    EXPOSE 8080

    CMD [ "node", "hello.js" ]
    ```

2. We need to create a [`Dockerfile`](exercise-01/Dockerfiles) (name is case sensitive) in the Cloud Shell with the above content in the same folder where hello.js program exists. Use any of the technique explained above for creating hello.js.

3. Check if the file is created properly by printing the contents using the below command in the shell prompt

   ``` sh
   cat Dockerfile
   ```

4. Now execute the docker build command to create docker image. It will create an image with name **cna-hello** with version **1.0**  (Please make sure the last parameter is a '`.`' indicating the current folder where the Dockerfile is present)

    ``` sh
    docker build -t cna-hello:1.0 .
    ```

5. Observe the console for the details of the steps taken while building the image. You should see successfully message as below.

     ``` text
     Sending build context to Docker daemon  3.072kB
     Step 1/5 : FROM node:alpine
     alpine: Pulling from library/node
     e6b0cf9c0882: Already exists
     ab436df1df6f: Pull complete
     470300a8a365: Pull complete
     84e7c11579ee: Pull complete
     Digest: sha256:b3f6a315aedc31ef3958108ce4926a52b4b4bcc93fca9655737d057de54f8157
     Status: Downloaded newer image for node:alpine
      ---> e1495e4ac50d
     Step 2/5 : WORKDIR /app
      ---> Running in 130512dea978
     Removing intermediate container 130512dea978
      ---> 834ca40ff0a1
     Step 3/5 : ADD . /app
      ---> bd597335e1b6
     Step 4/5 : EXPOSE 8080
      ---> Running in 2d5b7ac3548e
     Removing intermediate container 2d5b7ac3548e
      ---> 0661bc2f1c88
     Step 5/5 : CMD [ "node", "hello.js" ]
      ---> Running in 221ceff4dac6
     Removing intermediate container 221ceff4dac6
      ---> a2bcfc56c127
     Successfully built a2bcfc56c127
     Successfully tagged cna-hello:1.0
     ```

6. Check if the image is created and tagged properly by issuing the below command

    ```sh
    docker images
    ```

7. You should be seeing contents like below with different image id.

    ``` text
    REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
    cna-hello           1.0                 a2bcfc56c127        4 minutes ago       111MB
    node                alpine              e1495e4ac50d        13 days ago         111MB

    ```

8. Congratulations. You have now containerized your first application.

## Running the Hello World container ##

In the above step we have created a container image. Now let us run the container using its image.

1. First check if any container is already running by issuing the following command in the Shell prompt

    ```sh
    docker ps
    ```

2. This should return empty listing as we have not started any container

3. Start the container using the below command in the Shell prompt. It will print a long alphanumeric value and return to command prompt. The `-d` argument makes it run in detached/background mode. The `-p` is for port mapping. Here port `8080` of the host machine is mapped to port `8080` within the container.

    ``` sh
    docker run -d -p 8080:8080 cna-hello:1.0
    ```

4. Check again the container status using the `docker ps` command. You should be seeing the details of the container instance that is running.

    ``` text
    CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
    a1526b6d8c0e        cna-hello:1.0       "docker-entrypoint.s…"   12 seconds ago      Up 11 seconds       0.0.0.0:8080->8080/tcp   admiring_wilbur
    ```

5. Let us now test the running container instance. The container is currently running in the VM that is in Google Cloud and listening to port 8080. Use the same Web preview option to check, similar to how it was done while directly checking the Node.js program.

   You should be seeing Hello World in the new browser window.

   ![Preview Hello](img/preview-hello.png)

6. Stop the container using either the name or the id

    ``` sh
    docker stop admiring_wilbur
    # or
    docker stop a1526b6d8c0e
    ```

   Ensure that the container is stopped as the container is binding port `8080` which will be required in future steps

## Run in kubernetes ##

To run the container in kubernetes we have to first publish our container image to a docker registry which is accessible from the kubernetes cluster. In our case we will use the publicly available [Docker Hub](https://hub.docker.com/) service provided by [Docker](https://www.docker.com/)

### Push to Docker Hub ###

1. Sign up for a new Docker Hub ID at [https://hub.docker.com/signup](https://hub.docker.com/signup)
2. Once signed up login to the registry from the command line in Google Cloud Shell

    ```sh
    docker login
    ```

3. Enter the credentials for your Docker Hub ID when prompted. You should see the **Login Succeeded** message on the console which confirms your successful login
4. Now, since the image that was built earlier is not namespaced (i.e. cna-hello:1.0) we have to tag it under our namespace (your Docker Hub ID is your namespace). We can do that by running the following command

    ```sh
    docker tag cna-hello:1.0 <Docker Hub ID>/cna-hello:1.0
    ```

   Make sure to replace `<Docker Hub ID>` with your own Docker Hub ID wihtout the angle brackets

5. Now, we can push this container image (&lt;Docker Hub ID&gt;/cna-hello:1.0) to Docker Hub using the command

    ```sh
    docker push <Docker Hub ID>/cna-hello:1.0
    ```

   You should see something similar to this in your console

    ``` text
    The push refers to repository [docker.io/<Docker Hub ID>/cna-hello]
    bdf87701a1de: Pushed
    246024c2c409: Pushed
    efd6e0da275f: Pushed
    b352b61d0fe4: Pushed
    d06ff5e5272b: Pushed
    6b27de954cca: Pushed
    1.0: digest: sha256:412166826c2093fc08a53e6f506a4a583482d1b1b43aaf1637f75fcd554afa32 size: 1572
    ```

### Run container in kubernetes ###

Now, that the container has been pushed to Docker Hub we can run it in kubernetes. Ensure that you have completed the steps from [Connecting to the Kubernetes Cluster](connect-to-k8s.md) before you attempt to run any of the following instructions.

1. Run the following command replacing the `<Docker Hub ID>` with your own Docker Hub ID

    ```sh
    kubectl run cna-hello --generator=run-pod/v1 --image=<Docker Hub ID>/cna-hello:1.0
    ```

2. The above command should respond with the following response

    ``` text
    pod/cna-hello created
    ```

   We can also verify that the pod is running by executing

    ```sh
    kubectl get pods
    ```

   Which should result in something like below

    ``` text
    NAME        READY   STATUS    RESTARTS   AGE
    cna-hello   1/1     Running   0          3m28s
    ```

3. We can get more detailed information on the pod by running

    ```sh
    kubectl describe pod/cna-hello
    ```

   Which will display the image details and the events leading up to the creation of the pod

4. We can view the logs from the container running inside the pod by executing

    ```sh
    kubectl logs pod/cna-hello
    ```

   Which should give the

   ``` text
   Server running at 8080
   ```

5. We can delete the pod by running

    ```sh
    kubectl delete pod/cna-hello
    ```

**NOTE**: Learn more about the kubectl command [here][kubectl-cli]

## Kubernetes Resources ##

![Exercise 01](img/ex-01.png)

[kubectl-cli]: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands
[docker-cli]: https://docs.docker.com/engine/reference/commandline/cli/
[nodejs]: https://nodejs.org/en/about/
