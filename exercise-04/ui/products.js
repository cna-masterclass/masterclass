(function() {
  var pageSize = 3;
  var page = 1;
  var filter = '';
  function populate(products) {
    products.forEach((product, i) => {
      document.getElementById(`p${i}`).innerHTML = `<div class="product">
  <h1>${product.name}</h1>
  <div>
    <img alt="${product.name}" title="${product.name}" src="/img/${product.url}"/>
    <table class="product">
      <tr>
        <th>Category</th>
        <td>${product.category}</td>
      </tr>
      <tr>
        <th>Brand</th>
        <td>${product.brand}</td>
      </tr>
      <tr>
        <th>Price</th>
        <td>$&nbsp;${product.price}</td>
      </tr>
    </table>
  </div>
</div>`;
    })
  }
  function load(page) {
    fetch(`/products?q=${filter}&_limit=${pageSize}&_page=${page}`)
      .then(res => res.json())
      .then(j => {
        populate(j)
      })
      .catch(e => {
        document.getElementById('error').innerHTML = '<h2>Failed to load products</h2>';
      })
  }
  window.addEventListener('load', () => {
    load(page);
    document.getElementById('prev').addEventListener('click', () => {
      if ( page > 1 ) {
        page--;
        load(page)
      }
    })
    document.getElementById('next').addEventListener('click', () => {
      page++;
      load(page)
    })
    document.getElementById('search').addEventListener('click', () => {
      filter = document.getElementById('q').value;
      page = 1;
      load(page)
    })
  })
})()
